# README #

NodeLogin v1.0 por Emilio Hernández Pérez para AEG
Sistema de autentificación de usuarios en Node.JS desde una aplicación Android.

Versión preliminar ALPHA para testear el funcionamiento del server con Node.js.
Falta documentarla, añadir las dimensiones de los layout al archivo de dimens y adaptar las imágenes a todas las posibles resoluciones/densidades.

![Screenshot_2016-12-12-18-09-52.png](https://bitbucket.org/repo/nzjAbG/images/2369462700-Screenshot_2016-12-12-18-09-52.png)
![Screenshot_2016-12-12-18-11-33.png](https://bitbucket.org/repo/nzjAbG/images/2632123494-Screenshot_2016-12-12-18-11-33.png)
![Screenshot_2016-12-12-18-12-37.png](https://bitbucket.org/repo/nzjAbG/images/1757423962-Screenshot_2016-12-12-18-12-37.png)


### Funciones actuales ###

* Registro de usuarios
* Verificación de cuentas y opciones de resteo de password olvidado y solicitud de baja por e-mail.
* Gestión de cuenta: edición de datos personales, imagen de usuario, etc. y baja de la cuenta.
* Administración: El primer usuario en registrarse es administrador y dispone de la opción de editar todos los datos de los usuarios, incluyendo su foto, e-mail, nombre, estado de la cuenta (activar o desactivar) y borrar la cuenta.


### Información importante sobre el módulo SHARP de Node.js ###
Para instalar el modulo sharp, este debe compilarse, por lo que es preciso disponer de Visual C++ Build Tools y Python 2.7 instalado en el equipo.
Para hacer una instalación automática: 
```
#!

npm install -g windows-build-tools
```


Si a pesar de la instalación, da error de archivo "**CL.EXE**" durante la compilación,e ntonces debe instalarse el compilador C++ manualmente:
[http://go.microsoft.com/fwlink/?LinkId=691126](Link URL)


### Configuración del servidor ###
Para la configuración del servidor hay que generar el archivo "**config.json**" dentro de la carpeta "**/Server/config/**". Se incluye un archivo de muestra en "**/Server/config/config_ejemplo.json**"

Ejemplo de configuración del cron de limpieza de imágenes:


```
#!

"* * * * * *"
Segundos (0-59), Minutos (0-59), Horas (0-23), Día del mes (1-31), Mes (1-12), Día de la semana (1-7)
```

Ejemplos: 

```
#!

"30 * * * * *" - El cron se ejecutará cada minuto, en el segundo 30.
```

```
#!

"* 30 * * * *" - El cron se ejecutará cada hora, en el minuto 30.
```


```
#!

"00 00 10 * * 1-5" - El cron se ejecutará a diario de lunes a viernes a las 10:00h
```


### Configuración de la aplicación ###
La aplicación está configurada para reconocer los links que apunten al servidor Node Login e interactuar con su información (rellenar automáticamente campos de formularios de la app desde links del navegador o del correo). Para su correcto funcionamiento es preciso configurar la URL del servidor, que debe coincidir con la indicada en la configuración del servidor.
En "**strings.xml**" debe indicarse el campo "**url**".
En "**Constantes.java**" deben rellenarse los campos "**BASE_URL**", "**IMAGE_FOLDER**" y "**PUBLIC_PROFILE**" con los datos que se desee utilizar.