package es.topori.nodelogin.model;

public class RespuestaServidor {

    private String message;
    private String token;

    public String getRespuesta() { return message; }

    public String getToken() {
        return token;
    }
}
