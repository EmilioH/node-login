package es.topori.nodelogin.fragments;


import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.topori.nodelogin.R;
import es.topori.nodelogin.model.RespuestaServidor;
import es.topori.nodelogin.model.Usuario;
import es.topori.nodelogin.network.InstanciaRetorfit;
import es.topori.nodelogin.otros.Snackbars;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static es.topori.nodelogin.otros.Validaciones.validaEmail;
import static es.topori.nodelogin.otros.Validaciones.validaNombre;
import static es.topori.nodelogin.otros.Validaciones.validaPassword;

public class ResetPasswordDialog extends DialogFragment {

    public static final String TAG = ResetPasswordDialog.class.getSimpleName();

    @BindView(R.id.txt_email) EditText mEdtEmail;
    @BindView(R.id.edt_token) EditText mEdtToken;
    @BindView(R.id.edt_password) EditText mEdtPassword;

    @BindView(R.id.btn_reset) Button mBtnResetPassword;

    @BindView(R.id.bar_progreso) ProgressBar mBarProgreso;

    @BindView(R.id.txt_aviso) TextView mTxtAviso;

    @BindView(R.id.inp_email) TextInputLayout mInpEmail;
    @BindView(R.id.inp_token) TextInputLayout mInpToken;
    @BindView(R.id.inp_password) TextInputLayout mInpPassword;

    private Snackbars snackbar = new Snackbars();
    private Snackbar snack;
    private CompositeSubscription mSubscriptions;
    private Unbinder unbinder;
    private String mEmail;

    private boolean primerPaso = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_reset_password,container,false);
        unbinder = ButterKnife.bind(this, view);
        mSubscriptions = new CompositeSubscription();
        try {
            if (getDialog().getWindow() != null) {
                getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @OnClick(R.id.btn_reset)
    void resetPassword() {
            if (primerPaso) resetPasswordInit();
            else resetPasswordFinish();
        }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void setEmptyFields() {
        mInpEmail.setError(null);
        mInpToken.setError(null);
        mInpPassword.setError(null);
        mTxtAviso.setText(null);
    }

    public void setToken(String token) {
        mEdtToken.setText(token);
    }

    private void resetPasswordInit() {
        setEmptyFields();
        mEmail = mEdtEmail.getText().toString();
        int err = 0;
        if (!validaEmail(mEmail)) {
            err++;
            mInpEmail.setError(getString(R.string.email_incorrecto));
        }
        if (err == 0) {
            mBarProgreso.setVisibility(View.VISIBLE);
            resetPasswordInitProgress(mEmail);
        }
    }

    private void resetPasswordFinish() {
        setEmptyFields();
        String token = mEdtToken.getText().toString();
        String password = mEdtPassword.getText().toString();

        int err = 0;

        if (!validaNombre(token)) {
            err++;
            mInpToken.setError(getString(R.string.token_vacio));
        }
        if (!validaPassword(password)) {
            err++;
            mInpEmail.setError(getString(R.string.password_corto));
        }
        if (err == 0) {
            mBarProgreso.setVisibility(View.VISIBLE);
            Usuario usuario = new Usuario();
            usuario.setPassword(password);
            usuario.setToken(token);
            resetPasswordFinishProgress(usuario);
        }
    }

    private void resetPasswordInitProgress(String email) {

        mSubscriptions.add(InstanciaRetorfit.getRetrofit().resetPassword(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RespuestaServidor>() {
                    @Override
                    public final void onCompleted() {  }

                    @Override
                    public final void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public final void onNext(RespuestaServidor respuestaServidor) {
                        handleResponse(respuestaServidor);
                    }
                }));
    }

    private void resetPasswordFinishProgress(Usuario usuario) {

        mSubscriptions.add(InstanciaRetorfit.getRetrofit().resetPasswordFin(mEmail, usuario)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RespuestaServidor>() {
                    @Override
                    public final void onCompleted() {  }

                    @Override
                    public final void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public final void onNext(RespuestaServidor respuestaServidor) {
                        handleResponse(respuestaServidor);
                    }
                }));
    }

    private void handleResponse(RespuestaServidor respuestaServidor) {
        mBarProgreso.setVisibility(View.GONE);
        if (primerPaso) {
            primerPaso = false;
            showMessage(respuestaServidor.getRespuesta());
            mInpEmail.setVisibility(View.GONE);
            mInpToken.setVisibility(View.VISIBLE);
            mInpPassword.setVisibility(View.VISIBLE);

        } else {
            snack = snackbar.showSnackBarMessage(getView(), respuestaServidor.getRespuesta(), true);
            snack.show();
            snack.setCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    dismiss();
                }
                @Override
                public void onShown(Snackbar snackbar) {

                }
            });
        }
    }

    private void handleError(Throwable error) {
        mBarProgreso.setVisibility(View.GONE);
        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                RespuestaServidor respuestaServidor = gson.fromJson(errorBody,RespuestaServidor.class);
                showMessage(respuestaServidor.getRespuesta());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            showMessage(getString(R.string.error_red));
        }
    }

    private void showMessage(String message) {

        mTxtAviso.setVisibility(View.VISIBLE);
        mTxtAviso.setText(message);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
