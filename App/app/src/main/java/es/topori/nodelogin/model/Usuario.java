package es.topori.nodelogin.model;


import java.util.ArrayList;
import java.util.List;

public class Usuario {

    private String nombre;
    private String email;
    private String password;
    private String fecha_registro;
    private String ultimo_login;
    private String newPassword;
    private String token;
    private String imagen;
    private Boolean administrador = false;
    private Boolean activo = false;
    public final List<String> children = new ArrayList<String>();

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setImage(String imagen) { this.imagen = imagen; }

    public String getNombre() { return nombre; }

    public String getEmail() {
        return email;
    }

    public String getImage() {
        return imagen;
    }

    public String getFecha_registro() { return fecha_registro; }

    public String getUltimo_login() { return ultimo_login; }

    public Boolean getAdministrador() {
        return administrador;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) { this.activo = activo; }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setChildren(String children) {
        this.children.add(children);
    }
}
