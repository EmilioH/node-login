package es.topori.nodelogin.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.topori.nodelogin.activities.MainActivity;
import es.topori.nodelogin.R;
import es.topori.nodelogin.model.RespuestaServidor;
import es.topori.nodelogin.model.Usuario;
import es.topori.nodelogin.otros.Constantes;
import es.topori.nodelogin.otros.Snackbars;
import es.topori.nodelogin.network.InstanciaRetorfit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static es.topori.nodelogin.otros.Validaciones.validaNombre;

public class BajaFichaDialog extends DialogFragment {
    public static final String TAG = BajaFichaDialog.class.getSimpleName();

    @BindView(R.id.btn_confirmar) Button mBtnConfirmar;
    @BindView(R.id.btn_cancelar) Button mBtnCancel;

    @BindView(R.id.bar_progreso) ProgressBar mBarProgreso;
    @BindView(R.id.txt_baja) TextView mTxtBaja;

    @BindView(R.id.inp_token) TextInputLayout mInpToken;
    @BindView(R.id.edt_token) EditText mEdtToken;

    private CompositeSubscription mSubscriptions;

    private String mToken,mEmail;
    private Boolean mBaja = false;
    private Unbinder unbinder;
    private Snackbars snackbar = new Snackbars();
    private SharedPreferences.Editor mEditor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_baja,container,false);
        unbinder = ButterKnife.bind(this, view);
        mSubscriptions = new CompositeSubscription();
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mEditor = mSharedPreferences.edit();
        if (getArguments() != null) {
            try {
                mToken = getArguments().getString(Constantes.TOKEN);
                mEmail = getArguments().getString(Constantes.EMAIL);
                mBaja = getArguments().getBoolean(Constantes.BAJA);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            if (getDialog().getWindow() != null) {
                getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mBaja) {
            mTxtBaja.setText(getString(R.string.texto_paso2));
            mInpToken.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @OnClick(R.id.btn_confirmar)
    void bajaFicha() {
        if (mBaja) {
            segundoPaso();
        } else {
            primerPaso();
        }
    }


    @OnClick(R.id.btn_cancelar)
    void cancela() {
        if (mBaja) {
            mEditor.putBoolean(Constantes.BAJA, false);
            mEditor.commit();
        }
        dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    private void handleError(Throwable error) {
        mBarProgreso.setVisibility(View.GONE);
        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                RespuestaServidor respuestaServidor = gson.fromJson(errorBody,RespuestaServidor.class);
                snackbar.showSnackBarMessage(getView(), respuestaServidor.getRespuesta());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            snackbar.showSnackBarMessage(getView(), getString(R.string.error_red));
        }
    }

    private void primerPaso() {
        mBarProgreso.setVisibility(View.VISIBLE);
        mBtnCancel.setVisibility(View.INVISIBLE);
        mBtnConfirmar.setVisibility(View.INVISIBLE);
        mSubscriptions.add(InstanciaRetorfit.getRetrofit(mToken).borraCuenta(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RespuestaServidor>() {
                    @Override
                    public final void onCompleted() {
                    }

                    @Override
                    public final void onError(Throwable e) {
                        mBarProgreso.setVisibility(View.GONE);
                        mBtnCancel.setVisibility(View.VISIBLE);
                        mBtnConfirmar.setVisibility(View.VISIBLE);
                        handleError(e);
                    }

                    @Override
                    public final void onNext(RespuestaServidor respuestaServidor) {
                        dismiss();
                        mEditor.putBoolean(Constantes.BAJA, true);
                        mEditor.apply();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }));
    }

    private void segundoPaso() {
        String token = mEdtToken.getText().toString();
        if (!validaNombre(token)) {
            mInpToken.setError(getString(R.string.token_vacio));
        } else {
            mBtnCancel.setVisibility(View.GONE);
            mBtnConfirmar.setVisibility(View.GONE);
            mBarProgreso.setVisibility(View.VISIBLE);
            Usuario usuario = new Usuario();
            usuario.setToken(token);
            mSubscriptions.add(InstanciaRetorfit.getRetrofit(mToken).borraCuenta(mEmail, usuario)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<RespuestaServidor>() {
                        @Override
                        public final void onCompleted() { }

                        @Override
                        public final void onError(Throwable e) {
                            mBtnCancel.setVisibility(View.VISIBLE);
                            mBtnConfirmar.setVisibility(View.VISIBLE);
                            mBarProgreso.setVisibility(View.GONE);
                            handleError(e);
                        }

                        @Override
                        public final void onNext(RespuestaServidor respuestaServidor) {
                            mBarProgreso.setVisibility(View.GONE);
                            mEditor.putString(Constantes.EMAIL,"");
                            mEditor.putString(Constantes.TOKEN,"");
                            mEditor.putString(Constantes.IMAGEN,"");
                            mEditor.putString(Constantes.NAME,"");
                            mEditor.putBoolean(Constantes.BAJA,false);
                            mEditor.apply();
                            Snackbar snack = snackbar.showSnackBarMessage(getView(), respuestaServidor.getRespuesta(), true);
                            snack.show();
                            snack.setCallback(new Snackbar.Callback() {
                                @Override
                                public void onDismissed(Snackbar snackbar, int event) {
                                    getActivity().finish();
                                }
                                @Override
                                public void onShown(Snackbar snackbar) {
                                }
                            });
                        }
                    }));
        }
    }

    public void setToken(String token) {
        mEdtToken.setText(token);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
