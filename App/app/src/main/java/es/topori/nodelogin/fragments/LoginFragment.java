package es.topori.nodelogin.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import es.topori.nodelogin.activities.FichaActivity;
import es.topori.nodelogin.R;
import es.topori.nodelogin.model.RespuestaServidor;
import es.topori.nodelogin.network.InstanciaRetorfit;
import es.topori.nodelogin.otros.Constantes;
import es.topori.nodelogin.otros.Snackbars;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static es.topori.nodelogin.otros.Validaciones.validaEmail;
import static es.topori.nodelogin.otros.Validaciones.validaNombre;

public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();
    private CompositeSubscription mSubscriptions;
    private SharedPreferences.Editor mEditor;
    private Snackbars snackbar = new Snackbars();
    private Unbinder unbinder;
    private String mToken, mEmail;

    @BindView(R.id.edt_password) EditText mEtPassword;
    @BindView(R.id.edt_email) EditText mEtEmail;

    @BindView(R.id.btn_login) Button mBtnLogin;
    @BindView(R.id.btn_registrar) Button mBtnRegistrar;

    @BindView(R.id.inp_email) TextInputLayout mInpEmail;
    @BindView(R.id.inp_password) TextInputLayout mInpPassword;

    @BindView(R.id.img_logo) ImageView mImgLogo;

    @BindView(R.id.txt_pass_olvidado) TextView mTxtPassOlvidado;
    @BindView(R.id.txt_appinfo) TextView mTxtInfo;

    @BindView(R.id.bar_progreso) ProgressBar mBarProgreso;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login,container,false);
        unbinder = ButterKnife.bind(this, view);
        mSubscriptions = new CompositeSubscription();
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mEditor = mSharedPreferences.edit();
        mToken = mSharedPreferences.getString(Constantes.TOKEN,"");
        mEmail = mSharedPreferences.getString(Constantes.EMAIL,"");
        Boolean mBaja = mSharedPreferences.getBoolean(Constantes.BAJA,false);
        Boolean mActiva = mSharedPreferences.getBoolean(Constantes.ACTIVA,true);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            mTxtInfo.setText(Html.fromHtml(getString(R.string.copyright),Html.FROM_HTML_MODE_LEGACY));
        } else {
            mTxtInfo.setText(Html.fromHtml(getString(R.string.copyright)));
        }
        if (mBaja) {
            dialogoBaja();
        } else if (!mActiva){
            dialogoActiva();
        } else {
            if (!mToken.equals("") && !mEmail.equals("")) {
                Intent intent = new Intent(getActivity(), FichaActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        }
        return view;
    }

    @OnClick(R.id.btn_login)
    void login() {
        mInpEmail.setError(null); mInpEmail.setErrorEnabled(false);
        mInpPassword.setError(null); mInpPassword.setErrorEnabled(false);

        String email = mEtEmail.getText().toString();
        String password = mEtPassword.getText().toString();
        int err = 0;
        if (!validaEmail(email)) {
            err++;
            mInpEmail.setErrorEnabled(true);
            mInpEmail.setError(getString(R.string.email_incorrecto));
        }
        if (!validaNombre(password)) {
            err++;
            mInpPassword.setErrorEnabled(true);
            mInpPassword.setError(getString(R.string.password_vacio));
        }
        if (err == 0) {
            try {
                if (getView() != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mBtnLogin.setVisibility(View.GONE);
            mBarProgreso.setVisibility(View.VISIBLE);
            loginProcess(email,password);
        }
    }
    @OnClick(R.id.btn_registrar)
    void registramos() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        RegistroFragment fragment = new RegistroFragment();
        ft.replace(R.id.lay_fragment,fragment, RegistroFragment.TAG);
        ft.commit();
    }

    @OnClick(R.id.txt_pass_olvidado)
    void recuperamos() {
        ResetPasswordDialog fragment = new ResetPasswordDialog();
        fragment.show(getFragmentManager(), ResetPasswordDialog.TAG);
    }

    @OnTextChanged(value = R.id.edt_password, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void passChange() {
        if (mInpPassword.getError() != null) {
            mInpPassword.setError(null);
            mInpPassword.setErrorEnabled(false);
        }
    }

    @OnTextChanged(value = R.id.edt_email, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void mailChange() {
        if (mInpEmail.getError() != null) {
            mInpEmail.setError(null);
            mInpEmail.setErrorEnabled(false);
        }
    }

    @OnFocusChange(value = R.id.edt_email)
    void mailValid() {
        String email = mEtEmail.getText().toString();
        if (!email.equals("")) {
            if (!validaEmail(email)) {
                mInpEmail.setErrorEnabled(true);
                mInpEmail.setError(getString(R.string.email_incorrecto));
            }
        }
    }

    private void loginProcess(String email, String password) {
        mSubscriptions.add(InstanciaRetorfit.getRetrofit(email, password).login()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RespuestaServidor>() {
                    @Override
                    public final void onCompleted() { }
                    @Override
                    public final void onError(Throwable e) {
                        handleError(e);
                    }
                    @Override
                    public final void onNext(RespuestaServidor respuestaServidor) {
                        handleResponse(respuestaServidor);
                    }
                }));
    }

    private void handleResponse(RespuestaServidor respuestaServidor) {
        mBarProgreso.setVisibility(View.GONE);
        mBtnLogin.setVisibility(View.VISIBLE);
        mEditor.putString(Constantes.TOKEN, respuestaServidor.getToken());
        mEditor.putString(Constantes.EMAIL, respuestaServidor.getRespuesta());
        mEditor.commit();

        mEtEmail.setText(null);
        mEtPassword.setText(null);

        Intent intent = new Intent(getActivity(), FichaActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void handleError(Throwable error) {
        mBarProgreso.setVisibility(View.GONE);
        mBtnLogin.setVisibility(View.VISIBLE);

        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                RespuestaServidor respuestaServidor = gson.fromJson(errorBody,RespuestaServidor.class);
                if (respuestaServidor.getRespuesta().equals(getString(R.string.cuenta_no_activada))) {
                    mEmail = mEtEmail.getText().toString();
                    mEditor.putString(Constantes.EMAIL, mEmail);
                    mEditor.putBoolean(Constantes.ACTIVA, false);
                    mEditor.commit();
                    dialogoActiva();
                }
                snackbar.showSnackBarMessage(getView(), respuestaServidor.getRespuesta());
            } catch (IOException e) {
                e.printStackTrace();
                snackbar.showSnackBarMessage(getView(),getString(R.string.error));
            }
        } else {
            snackbar.showSnackBarMessage(getView(),getString(R.string.error_red));
        }
    }


    private void dialogoBaja() {
        BajaFichaDialog fragment = new BajaFichaDialog();
        Bundle bundle = new Bundle();
        bundle.putString(Constantes.EMAIL,mEmail);
        bundle.putString(Constantes.TOKEN,mToken);
        bundle.putBoolean(Constantes.BAJA,true);
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(), BajaFichaDialog.TAG);
    }

    private void dialogoActiva() {
        ActivaCuentaDialog fragment = new ActivaCuentaDialog();
        Bundle bundle = new Bundle();
        bundle.putString(Constantes.EMAIL,mEmail);
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(), ActivaCuentaDialog.TAG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
