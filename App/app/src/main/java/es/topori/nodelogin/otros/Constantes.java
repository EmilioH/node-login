package es.topori.nodelogin.otros;

public class Constantes {
    public static final String BASE_URL = "http://192.168.1.55:5000/";
    public static final String IMAGE_FOLDER = "images/";
    public static final String PUBLIC_PROFILE = "profile.jpg";
    public static final String TOKEN = "token";
    public static final String IMAGEN = "imagen";
    public static final String NAME = "nombre";
    public static final String EMAIL = "email";
    public static final String BAJA = "baja";
    public static final String ACTIVA = "activa";
}
