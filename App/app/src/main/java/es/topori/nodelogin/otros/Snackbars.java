package es.topori.nodelogin.otros;

import android.support.design.widget.Snackbar;
import android.view.View;

public class Snackbars {
    public void showSnackBarMessage(View vista, String message) {
        Snackbar.make(vista,message,Snackbar.LENGTH_SHORT).show();
    }
    public Snackbar showSnackBarMessage(View vista, String message, Boolean callback) {
        Snackbar barra = Snackbar.make(vista,message,Snackbar.LENGTH_SHORT);
        return barra;
    }
}
