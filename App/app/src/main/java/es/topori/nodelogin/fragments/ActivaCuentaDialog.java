package es.topori.nodelogin.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.topori.nodelogin.R;
import es.topori.nodelogin.model.RespuestaServidor;
import es.topori.nodelogin.model.Usuario;
import es.topori.nodelogin.otros.Constantes;
import es.topori.nodelogin.otros.Snackbars;
import es.topori.nodelogin.network.InstanciaRetorfit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static es.topori.nodelogin.otros.Validaciones.validaNombre;

public class ActivaCuentaDialog extends DialogFragment {
    public static final String TAG = ActivaCuentaDialog.class.getSimpleName();

    @BindView(R.id.btn_confirmar) Button mBtnConfirmar;
    @BindView(R.id.btn_cancelar) Button mBtnCancel;

    @BindView(R.id.bar_progreso) ProgressBar mBarProgreso;
    @BindView(R.id.img_checked) ImageView mImgChecked;

    @BindView(R.id.inp_token) TextInputLayout mInpToken;
    @BindView(R.id.edt_token) EditText mEdtToken;

    private CompositeSubscription mSubscriptions;

    private String mEmail;
    private Unbinder unbinder;
    private Snackbars snackbar = new Snackbars();
    private SharedPreferences.Editor mEditor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_activa_cuenta,container,false);
        unbinder = ButterKnife.bind(this, view);
        mSubscriptions = new CompositeSubscription();
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mEditor = mSharedPreferences.edit();
        if (getArguments() != null) {
            try {
                mEmail = getArguments().getString(Constantes.EMAIL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            if (getDialog().getWindow() != null) {
                getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @OnClick(R.id.btn_confirmar)
    void bajaFicha() {
        String token = mEdtToken.getText().toString();
        if (!validaNombre(token)) {
            mInpToken.setError(getString(R.string.token_vacio));
        } else {
            mBtnCancel.setVisibility(View.GONE);
            mBtnConfirmar.setVisibility(View.GONE);
            mBarProgreso.setVisibility(View.VISIBLE);
            Usuario usuario = new Usuario();
            usuario.setToken(token);
            mSubscriptions.add(InstanciaRetorfit.getRetrofit().activaCuenta(mEmail, usuario)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<RespuestaServidor>() {
                        @Override
                        public final void onCompleted() { }

                        @Override
                        public final void onError(Throwable e) {
                            mBtnCancel.setVisibility(View.VISIBLE);
                            mBtnConfirmar.setVisibility(View.VISIBLE);
                            mBarProgreso.setVisibility(View.GONE);
                            handleError(e);
                        }

                        @Override
                        public final void onNext(RespuestaServidor respuestaServidor) {
                            mBarProgreso.setVisibility(View.GONE);
                            mImgChecked.setVisibility(View.VISIBLE);
                            mEditor.putBoolean(Constantes.ACTIVA,true);
                            mEditor.apply();
                            dismiss();
                        }
                    }));
        }
    }


    @OnClick(R.id.btn_cancelar)
    void cancela() {
        dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    private void handleError(Throwable error) {
        mBarProgreso.setVisibility(View.GONE);
        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                RespuestaServidor respuestaServidor = gson.fromJson(errorBody,RespuestaServidor.class);
                snackbar.showSnackBarMessage(getView(), respuestaServidor.getRespuesta());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            snackbar.showSnackBarMessage(getView(), getString(R.string.error_red));
        }
    }

    public void setToken(String token) {
        mEdtToken.setText(token);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
