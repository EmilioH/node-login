package es.topori.nodelogin.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import es.topori.nodelogin.R;
import es.topori.nodelogin.model.RespuestaServidor;
import es.topori.nodelogin.model.Usuario;
import es.topori.nodelogin.otros.Constantes;
import es.topori.nodelogin.network.InstanciaRetorfit;
import es.topori.nodelogin.otros.Snackbars;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static es.topori.nodelogin.otros.Validaciones.validaEmail;
import static es.topori.nodelogin.otros.Validaciones.validaNombre;
import static es.topori.nodelogin.otros.Validaciones.validaPassword;
import static es.topori.nodelogin.otros.Validaciones.validaPassword2;

public class RegistroFragment extends Fragment {

    public static final String TAG = RegistroFragment.class.getSimpleName();

    @BindView(R.id.edt_nombre) EditText mEdtNombre;
    @BindView(R.id.edt_password) EditText mEdtPassword;
    @BindView(R.id.edt_password2) EditText mEdtPassword2;
    @BindView(R.id.edt_email) EditText mEdtEmail;

    @BindView(R.id.btn_registrar) Button mBtnRegistro;

    @BindView(R.id.txt_login) TextView mTxtLogin;

    @BindView(R.id.inp_nombre) TextInputLayout mInpNombre;
    @BindView(R.id.inp_email) TextInputLayout mInpEmail;
    @BindView(R.id.inp_password) TextInputLayout mInpPassword;
    @BindView(R.id.inp_password2) TextInputLayout mInpPassword2;

    @BindView(R.id.bar_progreso) ProgressBar mBarProgreso;

    private Unbinder unbinder;
    private Snackbars snackbar = new Snackbars();
    private Snackbar snack;
    private CompositeSubscription mSubscriptions;
    private SharedPreferences.Editor mEditor;
    private String emailRegistro = "";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registro,container,false);
        unbinder = ButterKnife.bind(this, view);
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mEditor = mSharedPreferences.edit();
        mSubscriptions = new CompositeSubscription();
        return view;
    }


    @OnClick(R.id.txt_login)
    void volvemos() {
        goToLogin();
    }

    @OnClick(R.id.btn_registrar)
    void registramos() {
        register();
    }

    @OnTextChanged(value = R.id.edt_password, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void passChange() {
        if (mInpPassword.getError() != null) {
            mInpPassword.setError(null);
            mInpPassword.setErrorEnabled(false);
        }
        if (mInpPassword2.getError() != null) {
            mInpPassword2.setError(null);
            mInpPassword2.setErrorEnabled(false);
        }
    }

    @OnTextChanged(value = R.id.edt_email, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void mailChange() {
        if (mInpEmail.getError() != null) {
            mInpEmail.setError(null);
            mInpEmail.setErrorEnabled(false);
        }
    }

    @OnTextChanged(value = R.id.edt_nombre, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void nameChange() {
        if (mInpNombre.getError() != null) {
            mInpNombre.setError(null);
            mInpNombre.setErrorEnabled(false);
        }
    }


    @OnFocusChange(value = R.id.edt_nombre)
    void nameValid() {
        String name = mEdtNombre.getText().toString();
        if (!name.equals("")) {
            if (!validaNombre(name)) {
                mInpNombre.setErrorEnabled(true);
                mInpNombre.setError(getString(R.string.nombre_incorrecto));
            }
        }
    }

    @OnFocusChange(value = R.id.edt_email)
    void mailValid() {
        String email = mEdtEmail.getText().toString();
        if (!email.equals("")) {
            if (!validaEmail(email)) {
                mInpEmail.setErrorEnabled(true);
                mInpEmail.setError(getString(R.string.email_incorrecto));
            }
        }
    }

    @OnFocusChange(value = R.id.edt_password)
    void passValid() {
        String pass1 = mEdtPassword.getText().toString();
        String pass2 = mEdtPassword2.getText().toString();
        if (!pass1.equals("")) {
            if (!validaPassword(pass1)) {
                mInpPassword.setErrorEnabled(true);
                mInpPassword.setError(getString(R.string.password_corto));
            }

            if (!pass2.equals("")) {
                if (!validaPassword2(pass1,pass2)) {
                    mInpPassword2.setErrorEnabled(true);
                    mInpPassword2.setError(getString(R.string.password_desigual));
                }
            }

        }
    }

    @OnFocusChange(value = R.id.edt_password2)
    void passCoincide() {
        String pass1 = mEdtPassword.getText().toString();
        String pass2 = mEdtPassword2.getText().toString();
        if (!pass1.equals("") && !pass2.equals("")) {
            if (!validaPassword2(pass1,pass2)) {
                mInpPassword2.setErrorEnabled(true);
                mInpPassword2.setError(getString(R.string.password_desigual));
            }
        }
    }

    private void register() {
        mInpNombre.setError(null); mInpNombre.setErrorEnabled(false);
        mInpEmail.setError(null); mInpEmail.setErrorEnabled(false);
        mInpPassword.setError(null); mInpPassword.setErrorEnabled(false);
        mInpPassword2.setError(null); mInpPassword2.setErrorEnabled(false);

        String name = mEdtNombre.getText().toString();
        String email = mEdtEmail.getText().toString();
        String password = mEdtPassword.getText().toString();
        String password2 = mEdtPassword2.getText().toString();

        int err = 0;

        if (!validaNombre(name)) {
            err++;
            mInpNombre.setErrorEnabled(true);
            mInpNombre.setError(getString(R.string.nombre_incorrecto));
        }

        if (!validaEmail(email)) {
            err++;
            mInpEmail.setErrorEnabled(true);
            mInpEmail.setError(getString(R.string.email_incorrecto));
        }

        if (!validaPassword(password)) {
            err++;
            mInpPassword.setErrorEnabled(true);
            mInpPassword.setError(getString(R.string.password_corto));
        }

        if (!validaPassword2(password,password2)) {
            err++;
            mInpPassword2.setErrorEnabled(true);
            mInpPassword2.setError(getString(R.string.password_desigual));
        }

        if (err == 0) {
            Usuario usuario = new Usuario();
            usuario.setNombre(name);
            usuario.setEmail(email);
            usuario.setPassword(password);
            emailRegistro = email;
            mBtnRegistro.setVisibility(View.GONE);
            mBarProgreso.setVisibility(View.VISIBLE);
            registerProcess(usuario);
        } else {
            mBtnRegistro.setVisibility(View.VISIBLE);
        }
    }


    private void registerProcess(Usuario usuario) {
        mSubscriptions.add(InstanciaRetorfit.getRetrofit().registro(usuario)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RespuestaServidor>() {
                    @Override
                    public final void onCompleted() {
                        // Nada
                    }

                    @Override
                    public final void onError(Throwable e) { handleError(e); }

                    @Override
                    public final void onNext(RespuestaServidor respuestaServidor) {
                        handleResponse(respuestaServidor);
                    }
                }));
    }

    private void handleResponse(RespuestaServidor respuestaServidor) {
        mBarProgreso.setVisibility(View.GONE);

        if (getView() != null) {
            snack = snackbar.showSnackBarMessage(getView(), respuestaServidor.getRespuesta(), true);
            snack.show();
        }

        if (respuestaServidor.getRespuesta().equals(getString(R.string.registro_completado))) {
            snack.setCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    mEditor.putString(Constantes.EMAIL,emailRegistro);
                    mEditor.putBoolean(Constantes.ACTIVA,false);
                    mEditor.commit();
                    goToLogin();
                }
                @Override
                public void onShown(Snackbar snackbar) {

                }
            });
        } else {
            mBtnRegistro.setVisibility(View.VISIBLE);
        }
    }

    private void handleError(Throwable error) {
        mBarProgreso.setVisibility(View.GONE);
        mBtnRegistro.setVisibility(View.VISIBLE);
        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                RespuestaServidor respuestaServidor = gson.fromJson(errorBody,RespuestaServidor.class);
                if (getView() != null) {
                    snackbar.showSnackBarMessage(getView(), respuestaServidor.getRespuesta());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (getView() != null) {
                snackbar.showSnackBarMessage(getView(), getString(R.string.error_red));
            }
        }
    }

    private void goToLogin(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        LoginFragment fragment = new LoginFragment();
        ft.replace(R.id.lay_fragment, fragment, LoginFragment.TAG);
        ft.commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
