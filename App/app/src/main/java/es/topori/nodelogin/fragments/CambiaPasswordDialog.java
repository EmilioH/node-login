package es.topori.nodelogin.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import es.topori.nodelogin.R;
import es.topori.nodelogin.model.RespuestaServidor;
import es.topori.nodelogin.model.Usuario;
import es.topori.nodelogin.network.InstanciaRetorfit;
import es.topori.nodelogin.otros.Constantes;
import es.topori.nodelogin.otros.Snackbars;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static es.topori.nodelogin.otros.Validaciones.validaPassword;
import static es.topori.nodelogin.otros.Validaciones.validaPassword2;

public class CambiaPasswordDialog extends DialogFragment {
    public static final String TAG = CambiaPasswordDialog.class.getSimpleName();

    @BindView(R.id.lay_contenidoPass) RelativeLayout mLayContenidoPass;
    @BindView(R.id.edt_pass_viejo) EditText mEdtPassViejo;
    @BindView(R.id.edt_pass_nuevo) EditText mEdtPassNuevo;
    @BindView(R.id.edt_pass_nuevo2) EditText mEdtPassNuevo2;

    @BindView(R.id.inp_pass_viejo) TextInputLayout mInpPassViejo;
    @BindView(R.id.inp_pass_nuevo) TextInputLayout mInpPassNuevo;
    @BindView(R.id.inp_pass_nuevo2) TextInputLayout mInpPassNuevo2;

    @BindView(R.id.btn_password) Button mBtnCambiaPassword;
    @BindView(R.id.btn_cancelar) Button mBtnCancelar;

    @BindView(R.id.bar_progreso) ProgressBar mBarProgreso;


    private CompositeSubscription mSubscriptions;

    private String mToken;
    private String mEmail;
    private Unbinder unbinder;
    private Snackbars snackbar = new Snackbars();
    private SharedPreferences.Editor mEditor;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_cambia_password,container,false);
        unbinder = ButterKnife.bind(this, view);
        mSubscriptions = new CompositeSubscription();
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mEditor = mSharedPreferences.edit();
        if (getArguments() != null) {
            mToken = getArguments().getString(Constantes.TOKEN);
            mEmail = getArguments().getString(Constantes.EMAIL);
        }
        try {
            if (getDialog().getWindow() != null) {
                getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @OnClick(R.id.btn_password)
    void cambiaPassword() {
        mBtnCambiaPassword.setEnabled(false);
        mInpPassViejo.setError(null); mInpPassViejo.setErrorEnabled(false);
        mInpPassNuevo.setError(null); mInpPassNuevo.setErrorEnabled(false);
        mInpPassNuevo2.setError(null); mInpPassNuevo2.setErrorEnabled(false);

        String oldPassword = mEdtPassViejo.getText().toString();
        String newPassword = mEdtPassNuevo.getText().toString();
        String newPassword2 = mEdtPassNuevo2.getText().toString();

        int err = 0;

        if (!validaPassword(oldPassword)) {
            err++;
            mInpPassViejo.setErrorEnabled(true);
            mInpPassViejo.setError(getString(R.string.password_corto));
        }

        if (!validaPassword(newPassword)) {
            err++;
            mInpPassNuevo.setErrorEnabled(true);
            mInpPassNuevo.setError(getString(R.string.password_corto));
        }

        if (!validaPassword2(newPassword,newPassword2)) {
            err++;
            mInpPassNuevo2.setErrorEnabled(true);
            mInpPassNuevo2.setError(getString(R.string.password_desigual));
        }

        if (newPassword.equals(oldPassword)) {
            err++;
            mInpPassNuevo.setErrorEnabled(true);
            mInpPassNuevo.setError(getString(R.string.password_repetido));
        }

        if (err == 0) {
            Usuario usuario = new Usuario();
            usuario.setPassword(oldPassword);
            usuario.setNewPassword(newPassword);
            changePasswordProgress(usuario);
            mBarProgreso.setVisibility(View.VISIBLE);
        } else {
            mBtnCambiaPassword.setEnabled(true);
        }
    }

    @OnTextChanged(value = R.id.edt_pass_viejo, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void oldPass() {
        if (mInpPassViejo.getError() != null) {
            mInpPassViejo.setError(null);
            mInpPassViejo.setErrorEnabled(false);
        }
    }

    @OnTextChanged(value = R.id.edt_pass_nuevo, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void newPass() {
        if (mInpPassNuevo.getError() != null) {
            mInpPassNuevo.setError(null);
            mInpPassNuevo.setErrorEnabled(false);
        }
        if (mInpPassNuevo2.getError() != null) {
            mInpPassNuevo2.setError(null);
            mInpPassNuevo2.setErrorEnabled(false);
        }
    }

    @OnTextChanged(value = R.id.edt_pass_nuevo2, callback = OnTextChanged.Callback.TEXT_CHANGED)
    void newPass2() {
        if (mInpPassNuevo2.getError() != null) {
            mInpPassNuevo2.setError(null);
            mInpPassNuevo2.setErrorEnabled(false);
        }
    }

    @OnClick(R.id.btn_cancelar)
    void cancela() {
        dismiss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    private void changePasswordProgress(Usuario usuario) {
        mSubscriptions.add(InstanciaRetorfit.getRetrofit(mToken).changePassword(mEmail, usuario)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RespuestaServidor>() {
                    @Override
                    public final void onCompleted() {
                        // Nada
                    }

                    @Override
                    public final void onError(Throwable e) {
                        mBtnCambiaPassword.setEnabled(true);
                        handleError(e);
                    }

                    @Override
                    public final void onNext(RespuestaServidor respuestaServidor) {
                        mEditor.putString(Constantes.TOKEN,mToken);
                        mEditor.commit();
                        handleResponse(respuestaServidor);
                    }
                }));
    }

    private void handleResponse(RespuestaServidor respuestaServidor) {
        mBarProgreso.setVisibility(View.GONE);
        Snackbar snack;
        snack = snackbar.showSnackBarMessage(getView(), respuestaServidor.getRespuesta(), true);
        snack.show();
            snack.setCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) { dismiss(); }
                @Override
                public void onShown(Snackbar snackbar) {

                }
            });
        }



    private void handleError(Throwable error) {
        mBarProgreso.setVisibility(View.GONE);
        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                RespuestaServidor respuestaServidor = gson.fromJson(errorBody,RespuestaServidor.class);
                snackbar.showSnackBarMessage(mLayContenidoPass, respuestaServidor.getRespuesta());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            snackbar.showSnackBarMessage(getView(), getString(R.string.error_red));
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
