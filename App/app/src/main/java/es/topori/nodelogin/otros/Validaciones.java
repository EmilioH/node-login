package es.topori.nodelogin.otros;

import android.text.TextUtils;
import android.util.Patterns;

public class Validaciones {

    public static boolean validaNombre(String name){
        if ((TextUtils.getTrimmedLength(name) < 2) || (TextUtils.getTrimmedLength(name) > 30)) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validaPassword(String pass){
        if (TextUtils.getTrimmedLength(pass) < 6) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validaPassword2(String pass, String pass2){
        if (validaPassword(pass) && !pass.equals(pass2)) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean validaEmail(String string) {
        if (TextUtils.isEmpty(string) || !Patterns.EMAIL_ADDRESS.matcher(string).matches()) {
            return false;
        } else {
            return  true;
        }
    }
}
