package es.topori.nodelogin.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.topori.nodelogin.R;
import es.topori.nodelogin.adapters.ListadoDesplegableAdapter;
import es.topori.nodelogin.fragments.BajaFichaDialog;
import es.topori.nodelogin.fragments.CambiaPasswordDialog;
import es.topori.nodelogin.model.RespuestaServidor;
import es.topori.nodelogin.model.Usuario;
import es.topori.nodelogin.otros.Archivos;
import es.topori.nodelogin.otros.Constantes;
import es.topori.nodelogin.otros.ImagenCircular;
import es.topori.nodelogin.otros.Snackbars;
import es.topori.nodelogin.network.InstanciaRetorfit;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static es.topori.nodelogin.otros.Validaciones.validaEmail;
import static es.topori.nodelogin.otros.Validaciones.validaNombre;

public class FichaActivity extends AppCompatActivity {

    @BindView(R.id.lay_cabecera_ficha) RelativeLayout mLayFicha;
    @BindView(R.id.layout_botones) LinearLayout mLayBotones;
    @BindView(R.id.lay_cabecera_usuarios) LinearLayout mLayCabecera;

    @BindView(R.id.txt_nombre) TextView mTxtNombre;
    @BindView(R.id.txt_email) TextView mTxtEmail;
    @BindView(R.id.txt_fecha) TextView mTxtFecha;
    @BindView(R.id.txt_fecha2) TextView mTxtFecha2;

    @BindView(R.id.img_ficha) ImageView mImgFicha;
    @BindView(R.id.img_admin) ImageView mImgAdmin;

    @BindView(R.id.bar_progreso) ProgressBar mBarProgreso;
    @BindView(R.id.list_usuarios) ExpandableListView mListUsuarios;

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private CompositeSubscription mSubscriptions;

    private String mToken, mEmail, mImage, mNombre;

    private int mGrupoAbierto = -1;

    private Snackbars snackbar = new Snackbars();

    private ListadoDesplegableAdapter listAdapter;
    private ArrayList<Usuario> usuariosList;
    private static final int PICK_IMAGE = 100;
    private static final int PICK_IMAGE_ADMIN = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ficha);
        ButterKnife.bind(this);

        mLayFicha.setVisibility(View.INVISIBLE);
        mLayBotones.setVisibility(View.INVISIBLE);

        mSubscriptions = new CompositeSubscription();

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mEditor = mSharedPreferences.edit();

        String mImgUrl = Constantes.BASE_URL+ Constantes.IMAGE_FOLDER+ Constantes.PUBLIC_PROFILE;
        mToken = mSharedPreferences.getString(Constantes.TOKEN,"");
        mEmail = mSharedPreferences.getString(Constantes.EMAIL,"");
        mNombre = mSharedPreferences.getString(Constantes.NAME,"");
        mImage = mSharedPreferences.getString(Constantes.IMAGEN, mImgUrl);
        loadProfile();
    }

    @OnClick(R.id.btn_password)
    void muestraDialog() {
        CambiaPasswordDialog fragment = new CambiaPasswordDialog();
        Bundle bundle = new Bundle();
        bundle.putString(Constantes.EMAIL,mEmail);
        bundle.putString(Constantes.TOKEN,mToken);
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(), CambiaPasswordDialog.TAG);
    }

    @OnClick(R.id.btn_salir)
    void logout() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constantes.EMAIL,"");
        editor.putString(Constantes.TOKEN,"");
        editor.putString(Constantes.IMAGEN,"");
        editor.putString(Constantes.NAME,"");
        editor.apply();
        finish();
    }

    @OnClick(R.id.img_edita_imagen)
    void cargaFoto() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, getString(R.string.selecciona_imagen));
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    @OnClick(R.id.img_edita_nombre)
    void actualizaNombre() {
        final Dialog dialog = new Dialog(FichaActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_cambia_nombre);
        final EditText editText = (EditText) dialog.findViewById(R.id.edt_nombre);
        Button cancelar = (Button) dialog.findViewById(R.id.btn_cancelar);
        Button guardar = (Button) dialog.findViewById(R.id.btn_guardar);
        if (mNombre == null) { mNombre ="Nombre"; }
        editText.setText("");
        editText.append(mNombre);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mNombre=editText.getText().toString();
                if (validaNombre(mNombre)) {
                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                    editor.putString(Constantes.NAME, mNombre);
                    editor.apply();
                    mTxtNombre.setText(mNombre);
                    try {
                        Usuario usuario = new Usuario();
                        usuario.setNombre(mNombre);
                        mSubscriptions.add(InstanciaRetorfit.getRetrofit(mToken).updateNombre(mEmail, usuario)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.io())
                                .subscribe(new Subscriber<RespuestaServidor>() {
                                    @Override
                                    public final void onCompleted() { }

                                    @Override
                                    public final void onError(Throwable e) {
                                        handleError(e,false);
                                    }

                                    @Override
                                    public final void onNext(RespuestaServidor respuestaServidor) {
                                        snackbar.showSnackBarMessage(findViewById(R.id.lay_cabecera_ficha), respuestaServidor.getRespuesta());
                                    }
                                }));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                } else {
                    snackbar.showSnackBarMessage(findViewById(R.id.lay_cabecera_ficha), getString(R.string.nombre_incorrecto));
                }
            }
        });
        dialog.show();
    }

    @OnClick(R.id.btn_baja)
    void bajaFicha() {
        BajaFichaDialog fragment = new BajaFichaDialog();
        Bundle bundle = new Bundle();
        bundle.putString(Constantes.EMAIL,mEmail);
        bundle.putString(Constantes.TOKEN,mToken);
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(), BajaFichaDialog.TAG);
    }


    private void loadProfile() {
        mBarProgreso.setVisibility(View.VISIBLE);
        mSubscriptions.add(InstanciaRetorfit.getRetrofit(mToken).getFicha(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Usuario>() {
                    @Override
                    public final void onCompleted() {
                    }
                    @Override
                    public final void onError(Throwable e) {
                        handleError(e,true);
                    }

                    @Override
                    public final void onNext(Usuario response) {
                        handleResponse(response);
                    }
                }));
    }

    private void handleResponse(Usuario usuario) {
        mLayFicha.setVisibility(View.VISIBLE);
        mLayBotones.setVisibility(View.VISIBLE);
        mBarProgreso.setVisibility(View.GONE);
        mTxtNombre.setText(usuario.getNombre());
        mTxtEmail.setText(usuario.getEmail());
        mTxtFecha.setText(String.format(getString(R.string.cuenta_creada),usuario.getFecha_registro()));
        mTxtFecha2.setText(String.format(getString(R.string.ultimo_login), usuario.getUltimo_login()));
        mImage = usuario.getImage();
        mNombre = usuario.getNombre();
        mEmail = usuario.getEmail();
        if (mImage == null) {
            mImage= Constantes.PUBLIC_PROFILE;
        }

        mEditor.putString(Constantes.EMAIL, usuario.getEmail());
        mEditor.putString(Constantes.IMAGEN,mImage);
        mEditor.putString(Constantes.NAME, usuario.getNombre());
        mEditor.commit();
            Glide.with(FichaActivity.this)
                    .load(mImage.startsWith("content") ? Uri.parse(mImage) : Constantes.BASE_URL+Constantes.IMAGE_FOLDER+mImage)
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new ImagenCircular(FichaActivity.this))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(mImgFicha);

        if (usuario.getAdministrador()) {
            mImgAdmin.setVisibility(View.VISIBLE);
            mListUsuarios.setVisibility(View.VISIBLE);
            mLayCabecera.setVisibility(View.VISIBLE);
            mSubscriptions.add(InstanciaRetorfit.getRetrofit(mToken).getListado(mEmail)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<RespuestaServidor>() {
                        @Override
                        public final void onCompleted() {
                            // Nada
                        }

                        @Override
                        public final void onError(Throwable e) {
                            handleError(e,false);
                        }

                        @Override
                        public final void onNext(RespuestaServidor respuestaServidor) {
                            String respuesta = respuestaServidor.getRespuesta();
                            Gson gson = new Gson();
                            Type type = new TypeToken<List<Usuario>>(){}.getType();
                            usuariosList = gson.fromJson(respuesta, type);
                            for (Usuario object: usuariosList) {
                                object.setChildren(getString(R.string.modificar_imagen));
                                object.setChildren(getString(R.string.modificar_nombre));
                                object.setChildren(getString(R.string.modificar_email));
                                object.setChildren(getString(R.string.modificar_cuenta));
                                object.setChildren(getString(R.string.borrar_cuenta));
                            }
                            listAdapter = new ListadoDesplegableAdapter(FichaActivity.this, usuariosList);
                            mListUsuarios.setAdapter(listAdapter);

                            mListUsuarios.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                @Override
                                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                    mGrupoAbierto = groupPosition;
                                    Usuario group = (Usuario) listAdapter.getGroup(groupPosition);

                                        actualizaFichasAdmin(childPosition, group, group.getEmail());
                                    return false;
                                }
                            });

                            mListUsuarios.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                int previousGroup = -1;
                                @Override
                                public void onGroupExpand(int groupPosition) {
                                    if(groupPosition != previousGroup)
                                        mListUsuarios.collapseGroup(previousGroup);
                                    previousGroup = groupPosition;
                                }
                            });
                        }
                    }));
        }
    }

    private void handleError(Throwable error, Boolean cierra) {
        mBarProgreso.setVisibility(View.GONE);
        if (error instanceof HttpException) {
            Gson gson = new GsonBuilder().create();
            try {
                String errorBody = ((HttpException) error).response().errorBody().string();
                RespuestaServidor respuestaServidor = gson.fromJson(errorBody,RespuestaServidor.class);
                snackCierra(respuestaServidor.getRespuesta(), cierra);
            } catch (IOException e) {
                snackCierra(getString(R.string.error),cierra);
                e.printStackTrace();
            }
        } else {
            snackCierra(getString(R.string.error_red),cierra);
        }
    }

    private void snackCierra(final String texto, final Boolean cierra) {
        Snackbar snack;

        snack = snackbar.showSnackBarMessage(findViewById(R.id.activity_profile),texto, true);
        snack.show();
        snack.setCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                if (texto.equals(getString(R.string.token_incorrecto))) {
                    mEditor.putString(Constantes.EMAIL,"");
                    mEditor.putString(Constantes.TOKEN,"");
                    mEditor.putString(Constantes.IMAGEN,"");
                    mEditor.putString(Constantes.NAME,"");
                    mEditor.commit();
                    Intent intent = new Intent(FichaActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else if (cierra) {
                    finish();
                }
            }
            @Override
            public void onShown(Snackbar snackbar) {
            }
        });
    }


    public void actualizaFichasAdmin(final int tipo, final Usuario usuario, final String nuevoEmail) {
        if (tipo == 0) {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent chooserIntent = Intent.createChooser(getIntent, getString(R.string.selecciona_imagen));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

                startActivityForResult(chooserIntent, PICK_IMAGE_ADMIN);
        } else {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_administrador);
            final RelativeLayout mLayContenedor = (RelativeLayout) dialog.findViewById(R.id.lay_contenedor);
            TextView mTitulo = (TextView) dialog.findViewById(R.id.txt_titulo);
            TextView mTexto1 = (TextView) dialog.findViewById(R.id.txt_texto1);
            TextView mTexto2 = (TextView) dialog.findViewById(R.id.txt_texto2);
            final EditText mEdit1 = (EditText) dialog.findViewById(R.id.edt_edit1);
            final Button mBoton1 = (Button) dialog.findViewById(R.id.btn_boton1);
            final Button mBoton2 = (Button) dialog.findViewById(R.id.btn_boton2);
            mEdit1.setText("");
            if (tipo == 1) {
                if (usuario.getNombre() == null) {
                    usuario.setNombre("Nombre");
                }
                mEdit1.append(usuario.getNombre());
                mTitulo.setText(getString(R.string.modificar_nombre));
                mBoton1.setText(getString(R.string.cancelar));
                mBoton2.setText(getString(R.string.guardar));
                mTexto1.setVisibility(View.GONE);
                mTexto2.setVisibility(View.GONE);
            } else if (tipo == 2) {
                mEdit1.append(usuario.getEmail());
                mTitulo.setText(getString(R.string.modificar_email));
                mBoton1.setText(getString(R.string.cancelar));
                mBoton2.setText(getString(R.string.guardar));
                mTexto1.setText(getString(R.string.email_nuevo));
                mTexto2.setVisibility(View.GONE);
            } else if (tipo == 3) {
                mTitulo.setText(getString(R.string.modificar_cuenta));
                mBoton1.setText(getString(R.string.cancelar));
                mBoton2.setText(usuario.getActivo() ? getString(R.string.desactivar) : getString(R.string.activar));
                mEdit1.setVisibility(View.GONE);
                mTexto2.setText(usuario.getActivo() ? getString(R.string.activada) : getString(R.string.desactivada));
            } else if (tipo == 4) {
                mTitulo.setText(getString(R.string.borrar_cuenta));
                mBoton1.setText(getString(R.string.cancelar));
                mBoton2.setText(getString(R.string.confirmar));
                mEdit1.setVisibility(View.GONE);
                mTexto1.setText(getString(R.string.texto_baja));
                mTexto2.setVisibility(View.GONE);
            }


            mBoton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });

            mBoton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean completado = false;
                    String nuevoEmailTemp = nuevoEmail;
                    if (tipo == 1) {
                        String text = mEdit1.getText().toString();
                        if (validaNombre(text)) {
                            usuario.setNombre(text);
                            completado = true;
                        } else {
                            snackbar.showSnackBarMessage(mLayContenedor, getString(R.string.nombre_incorrecto));
                        }
                    } else if (tipo == 2) {
                        String text = mEdit1.getText().toString();
                        if (validaEmail(text)) {
                            nuevoEmailTemp = text;
                            completado = true;
                        } else {
                            snackbar.showSnackBarMessage(mLayContenedor, getString(R.string.email_incorrecto));
                        }
                    } else if (tipo == 3) {
                        usuario.setActivo(!usuario.getActivo());
                        completado = true;
                    } else if (tipo == 4) {
                        //Primer paso, le pedimos que confirme de nuevo
                        if (mBoton2.getText() == getString(R.string.confirmar)) {
                            mBoton2.setText(getString(R.string.confirmar2));
                        } else {
                            nuevoEmailTemp = getString(R.string.email_borrado);
                            completado = true;
                        }

                    }
                    if (completado) {
                        conexionAdmin(usuario, nuevoEmailTemp);
                        if (nuevoEmailTemp.equals(getString(R.string.email_borrado))) {
                            usuariosList.remove(usuario);
                            listAdapter.notifyDataSetChanged();
                        } else {
                            usuario.setEmail(nuevoEmailTemp);
                        }
                        dialog.dismiss();
                    }
                }
            });
            dialog.show();
        }
    }

    private void conexionAdmin(Usuario usuario, String nuevoEmail) {
        try {
            mSubscriptions.add(InstanciaRetorfit.getRetrofit(mToken).adminUpdateUser(mEmail, nuevoEmail, usuario)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<RespuestaServidor>() {
                        @Override
                        public final void onCompleted() { }

                        @Override
                        public final void onError(Throwable e) { }

                        @Override
                        public final void onNext(RespuestaServidor respuestaServidor) { }
                    }));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSubscriptions.unsubscribe();
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String descripcion = "";
        if (resultCode == RESULT_OK && (requestCode == PICK_IMAGE || requestCode == PICK_IMAGE_ADMIN)) {
            Uri imageUri = data.getData();
            if (requestCode == PICK_IMAGE) {
                Glide.with(FichaActivity.this)
                        .load(imageUri)
                        .crossFade()
                        .thumbnail(0.5f)
                        .bitmapTransform(new ImagenCircular(FichaActivity.this))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(mImgFicha);
            } else {
                if (mGrupoAbierto > -1) {
                    Usuario group = (Usuario) listAdapter.getGroup(mGrupoAbierto);
                    descripcion = group.getEmail();
                }
            }
            try {
                File file = new File(new Archivos().getRealPathFromURI(getApplicationContext(), data.getData()));
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
                RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), descripcion);
                    mSubscriptions.add(InstanciaRetorfit.getRetrofit(mToken).uploadImagen(mEmail, body, description)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Subscriber<RespuestaServidor>() {
                                @Override
                                public final void onCompleted() {
                                    // Nada
                                }

                                @Override
                                public final void onError(Throwable e) {
                                    handleError(e, false);
                                }

                                @Override
                                public final void onNext(RespuestaServidor respuestaServidor) {
                                    if (requestCode == PICK_IMAGE) {
                                        String respuesta = respuestaServidor.getRespuesta();
                                        mEditor.putString(Constantes.IMAGEN, respuesta);
                                        mEditor.commit();
                                    } else {
                                        if (mGrupoAbierto > -1) {
                                            String respuesta = respuestaServidor.getRespuesta();
                                            Usuario group = (Usuario) listAdapter.getGroup(mGrupoAbierto);
                                            group.setImage(respuesta);
                                            listAdapter.notifyDataSetChanged();
                                        }
                                    }
                                    }
                            }));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

