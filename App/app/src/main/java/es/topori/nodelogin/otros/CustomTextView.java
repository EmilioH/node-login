package es.topori.nodelogin.otros;

import es.topori.nodelogin.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import java.util.Arrays;

public class CustomTextView extends TextView {
    private TypedArray a;
    private String fontName;
    private boolean ajustar = false;
    private Typeface tf;
    float defaultTextSize = 0.0f;

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        readAttr(context,attrs);
        init();
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttr(context,attrs);
        init();
    }

    public CustomTextView(Context context) {
        super(context);
        init();
    }

    public void setTypeface(String fontName) {
        this.fontName = fontName;
        init();
    }

    private void init() {
        if (ajustar) {
            setSingleLine();
            setEllipsize(TextUtils.TruncateAt.END);
        }
        defaultTextSize = getTextSize();
        if (!isInEditMode()) {
            if (fontName != null) {
                tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/"+fontName);
            }
            setTypeface(tf);
            if (a != null) {
                a.recycle();
            }
        }
    }


    private void readAttr(Context context, AttributeSet attrs) {
        a = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        fontName = a.getString(R.styleable.CustomTextView_fuente);
        ajustar = a.getBoolean(R.styleable.CustomTextView_ajustar, false);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setTextSize(TypedValue.COMPLEX_UNIT_PX, defaultTextSize);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (ajustar) {
            final String[] yourArray;
            final Layout layout = getLayout();
            if (layout != null) {
                final int lineCount = layout.getLineCount();
                if (lineCount > 0) {
                    String texto = layout.getText().toString();
                    String[] parts = texto.split(" ");
                    if (parts[0].equalsIgnoreCase(("Bar"))) {
                        yourArray = Arrays.copyOfRange(parts, 1, parts.length);

                        StringBuilder builder = new StringBuilder();
                        for(String s : yourArray) {
                            builder.append(s).append(" ");
                        }
                        setText(builder.toString().trim());
                    }

                    int ellipsisCount = layout.getEllipsisCount(lineCount - 1);
                    while (ellipsisCount > 0) {
                        final float textSize = getTextSize();
                        setTextSize(TypedValue.COMPLEX_UNIT_PX, (textSize - 1));
                        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                        ellipsisCount = layout.getEllipsisCount(lineCount - 1);
                    }
                }
            }
        }
    }
}