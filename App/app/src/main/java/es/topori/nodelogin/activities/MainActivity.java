package es.topori.nodelogin.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import es.topori.nodelogin.R;
import es.topori.nodelogin.fragments.ActivaCuentaDialog;
import es.topori.nodelogin.fragments.BajaFichaDialog;
import es.topori.nodelogin.fragments.LoginFragment;
import es.topori.nodelogin.fragments.ResetPasswordDialog;

public class MainActivity extends AppCompatActivity {

    private LoginFragment mLoginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            if (savedInstanceState == null) {
                loadFragment();
            }
    }

    private void loadFragment(){
        if (mLoginFragment == null) {
            mLoginFragment = new LoginFragment();
        }
        getFragmentManager().beginTransaction().replace(R.id.lay_fragment,mLoginFragment,LoginFragment.TAG).commit();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            String data = intent.getData().getLastPathSegment();
            if (data.startsWith("_")) {
                BajaFichaDialog mBajaFichaDialog;
                mBajaFichaDialog = (BajaFichaDialog) getFragmentManager().findFragmentByTag(BajaFichaDialog.TAG);
                if (mBajaFichaDialog != null)
                    mBajaFichaDialog.setToken(data.substring(1));
            } else if (data.startsWith("*")) {
                ActivaCuentaDialog mActivaCuentaDialog;
                mActivaCuentaDialog = (ActivaCuentaDialog) getFragmentManager().findFragmentByTag(ActivaCuentaDialog.TAG);
                if (mActivaCuentaDialog != null)
                    mActivaCuentaDialog.setToken(data.substring(1));
            } else {
                ResetPasswordDialog mResetPasswordDialog;
                mResetPasswordDialog = (ResetPasswordDialog) getFragmentManager().findFragmentByTag(ResetPasswordDialog.TAG);
                if (mResetPasswordDialog != null)
                    mResetPasswordDialog.setToken(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
