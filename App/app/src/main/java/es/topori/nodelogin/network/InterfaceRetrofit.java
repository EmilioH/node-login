package es.topori.nodelogin.network;

import es.topori.nodelogin.model.RespuestaServidor;
import es.topori.nodelogin.model.Usuario;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;

public interface InterfaceRetrofit {

    @POST("usuarios")
    Observable<RespuestaServidor> registro(@Body Usuario usuario);

    @POST("loguear")
    Observable<RespuestaServidor> login();

    @GET("usuarios/{email}")
    Observable<Usuario> getFicha(@Path("email") String email);

    @GET("lista/{email}")
    Observable<RespuestaServidor> getListado(@Path("email") String email);

    @PUT("usuarios/{email}")
    Observable<RespuestaServidor> changePassword(@Path("email") String email, @Body Usuario usuario);

    @POST("usuarios/{email}")
    Observable<RespuestaServidor> borraCuenta(@Path("email") String email);

    @POST("usuarios/{email}")
    Observable<RespuestaServidor> borraCuenta(@Path("email") String email, @Body Usuario usuario);

    @POST("usuarios/{email}/activacion")
    Observable<RespuestaServidor> activaCuenta(@Path("email") String email, @Body Usuario usuario);

    @Multipart
    @POST("usuarios/{email}/imagen")
    Observable<RespuestaServidor> uploadImagen(@Path("email") String email, @Part MultipartBody.Part file, @Part("description") RequestBody description);

    @POST("usuarios/{email}/nombre")
    Observable<RespuestaServidor> updateNombre(@Path("email") String email, @Body Usuario usuario);

    @POST("usuarios/{email}/password")
    Observable<RespuestaServidor> resetPassword(@Path("email") String email);

    @POST("usuarios/{email}/password")
    Observable<RespuestaServidor> resetPasswordFin(@Path("email") String email, @Body Usuario usuario);

    @POST("admin/{email}/{nuevoEmail}")
    Observable<RespuestaServidor> adminUpdateUser(@Path("email") String email, @Path("nuevoEmail") String nuevoEmail, @Body Usuario usuario);
}
