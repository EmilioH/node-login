package es.topori.nodelogin.adapters;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;

import es.topori.nodelogin.R;
import es.topori.nodelogin.model.Usuario;
import es.topori.nodelogin.otros.Constantes;
import es.topori.nodelogin.otros.ImagenCircular;

public class ListadoDesplegableAdapter extends BaseExpandableListAdapter {

    private final ArrayList<Usuario> usuarios;
    private Activity actividad;
    private LayoutInflater mInflater;

    public ListadoDesplegableAdapter(Activity actividad, ArrayList<Usuario> usuarios) {
        this.actividad = actividad;
        this.usuarios = usuarios;
        mInflater = actividad.getLayoutInflater();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return usuarios.get(groupPosition).children.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String children = (String) getChild(groupPosition, childPosition);
        TextView text = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_usuarios_childs, null);
        }
        try {
            text = (TextView) convertView.findViewById(R.id.txt_opciones);
            text.setText(children);
            if (children.equals(actividad.getString(R.string.modificar_imagen))) {
                text.setCompoundDrawablesWithIntrinsicBounds(actividad.getBaseContext().getResources().getDrawable(R.drawable.ic_picture),null,null,null);
            } else if (children.equals(actividad.getString(R.string.modificar_nombre))) {
                text.setCompoundDrawablesWithIntrinsicBounds(actividad.getBaseContext().getResources().getDrawable(R.drawable.ic_person),null,null,null);
            } else if (children.equals(actividad.getString(R.string.modificar_email))) {
                text.setCompoundDrawablesWithIntrinsicBounds(actividad.getBaseContext().getResources().getDrawable(R.drawable.ic_email),null,null,null);
            } else if (children.equals(actividad.getString(R.string.modificar_cuenta))) {
                text.setCompoundDrawablesWithIntrinsicBounds(actividad.getBaseContext().getResources().getDrawable(R.drawable.ic_lock),null,null,null);
            } else if (children.equals(actividad.getString(R.string.borrar_cuenta))) {
                text.setCompoundDrawablesWithIntrinsicBounds(actividad.getBaseContext().getResources().getDrawable(R.drawable.ic_baja),null,null,null);
            }
        } catch (Exception e) {

        }
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return usuarios.get(groupPosition).children.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return usuarios.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return usuarios.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_usuarios, null);
        }
        final View convertView2 = convertView;

        Usuario group = (Usuario) getGroup(groupPosition);
        String mImage = group.getImage();
        Glide.with(actividad)
                .load(mImage.startsWith("content") ? Uri.parse(mImage) : Constantes.BASE_URL+Constantes.IMAGE_FOLDER+mImage)
                .crossFade()
                .thumbnail(0.15f)
                .bitmapTransform(new ImagenCircular(actividad))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new SimpleTarget<GlideDrawable>(50, 50) {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation glideAnimation) {
                        ((CheckedTextView) convertView2).setCheckMarkDrawable(resource);
                    }
                });
        ((CheckedTextView) convertView2).setText(group.getEmail());
        ((CheckedTextView) convertView2).setChecked(isExpanded);
        return convertView2;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}