'use strict';
const config = require('../config/config.json');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = mongoose.Schema({ 
	nombre 			: String,
	email			: String,
  hashed_password	: String,
	administrador	: Boolean,
	activo				: Boolean,
	fecha_registro	: String,
	ultimo_login		: String,
	imagen				: String,
	pass_temporal	: String,
	pass_temporal_time: String
});

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://'+config.mongouser+':'+config.mongopass+'@'+config.mongourl+':'+config.mongoport+'/'+config.mongodb);
module.exports = mongoose.model('user', userSchema);