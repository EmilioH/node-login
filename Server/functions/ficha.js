'use strict';

const user = require('../models/user');
const sharp = require('sharp');

exports.getFicha = email =>
	
	new Promise((resolve,reject) => {

		user.find({ email: email }, { nombre: 1, email: 1, administrador: 1, fecha_registro: 1, ultimo_login: 1, imagen: 1, _id: 0 })

		.then(users => resolve(users[0]))

		.catch(err => reject({ status: 500, message: 'Error del servidor' }))

	});
	
exports.updateImagen = (email, imagen) =>

	new Promise((resolve,reject) => {

			var fs = require('fs');
			//He tenido problemas diversos al cargarla desde el json de configuración, por eso está temporalmente "hardcoded"
			var filePath = './images/'; 
			var imagen2 = "t_"+imagen;
		user.find({ email: email })

		.then(users => {
			let user = users[0];
				return user;
		})

		.then(user => {

			//Borramos del servidor la imagen antigua del usuario al sustituirla por la nueva
			//En caso de no poder realizarse por estar en uso, se eliminará con la limpieza regular del cron		
			fs.stat(filePath+user.imagen, function(err,stats){
        if(!err){
        	if (!user.imagen.includes("ficha")) {
         		fs.unlink(filePath+user.imagen,function(err){
        		if(err) return console.log('Error borrando imagen vieja');
   					});
         	}
       	}
      });

		//Almacenamos la nueva imagen y creamos una miniatura, que es la que usaremos.
	   sharp(filePath+imagen)
  			.resize(300,300)
  			.max()
  			.toFile(filePath+imagen2)
  			.then()
		  	.catch();
			/*
			 * Tras realizar muchas pruebas usando la memoria para almacenar la foto y guardar solamente la imagen reducida,
			 * acabé descubriendo que podía suponer un problema serio de memoria para el sistema si se realizaban muchas subidas
			 * de imágenes en un corto espacio de tiempo. La solución inicial que tomé fue guardar las imágenes originales, luego
			 * reducirlas y posteriormente borrarlas, pero en muchas ocasiones el sistema tenía bloqueadas las imágenes originales
			 * durante un tiempo tras su uso, y al intentar el borrado finalizaba con un error.
			 * Finalmente decidí usar un cron que revisase la carpeta de imágenes cada hora y borrase las imágenes que no estén
			 * asociadas a ningún usuario.
			 * El cron se ejecuta con el inicio del sistema.
			*/
 				user.imagen = imagen2;
				return user.save();

		})
		.then(user => resolve({ status: 200, message: imagen2 }))

		.catch(err => reject({ status: 500, message: 'Error del servidor' }));

	});
	
exports.updateNombre = (email, nombre) =>

	new Promise((resolve,reject) => {

		user.find({ email: email })

		.then(users => {
			let user = users[0];
				return user;
		})

		.then(user => {
				user.nombre = nombre;
				return user.save();
		})
		.then(user => resolve({ status: 200, message: 'Nombre guardado correctamente' }))

		.catch(err => reject({ status: 500, message: 'Error del servidor' }));
	
	});