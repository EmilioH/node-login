'use strict';

const user = require('../models/user');
const dateFormat = require('dateformat');
const encriptar = require('./encriptar');
const nodemailer = require('nodemailer');
const config = require('../config/config.json');

exports.registraUsuario = (nombre, email, password) =>

	new Promise((resolve,reject) => {
	const random = textoAleatorio(8);
	var admin = false;
	
			//Al primer usuario en registrarse, se le dan poderes de administración
			user.count(function(err, count) {
	      if (count == 0) {
	      	admin = true;
	      }
    	})
          
		user.find({ email: email })

		.then(users => {
			if (users.length > 0) {

				reject({ status: 409, message: 'El usuario ya existe' });

			} else {

         if (/(.)\1{2,}/.test(password)) {

           reject({ status: 409, message: 'El Password no puede tener 3 caracteres iguales seguidos' });

         } else if (password.length > 10) {

         	reject({ status: 409, message: 'El Password no puede superar los 10 caracteres.' });
				 } else if (password.length < 6) {
				 	reject({ status: 409, message: 'El Password debe tener al menos 6 caracteres.' });

				 } else {

					var now = dateFormat(new Date(), "dd/mm/yyyy HH:MM:ss");

					const hash = encriptar.encrypt(password);
					const hashToken = encriptar.encrypt(random);

					const transporter = nodemailer.createTransport(`smtps://${config.email}:${config.password}@smtp.gmail.com`);

					const mailOptions = {

		    			from: `"${config.nombre}" <${config.email}>`,
		    			to: email,  
		    			subject: `Confirma tu cuenta en ${config.nombre}`, 
		    			html: `Hola ${nombre},<br><br>
		    			Gracias por registrarte en ${config.nombre}.<br><br>
		    			Para continuar con el registro, si est&aacute;s viendo este e-mail desde el dispositivo con NodeLogin, haz click <a href = "http://${config.url}/*${random}">AQU&Iacute;</a>, de lo contrario introduce el siguiente Token en la APP: ${random}<br><br>
		    			Un saludo.`
					};

					transporter.sendMail(mailOptions);


          
					const nuevoUsuario = new user({
						nombre: nombre,
						email: email,
						hashed_password: hash,
						administrador: admin,
						activo: false,
						imagen: config.default,
						fecha_registro: now,
						ultimo_login: now,
						pass_temporal: hashToken,
						pass_temporal_time: new Date()
					});
					nuevoUsuario.save()
		
		
		.then(() => resolve({ status: 201, message: 'Registro completado' }))
		.catch(err => {
			if (err.code == 11000) {
				reject({ status: 409, message: 'El usuario ya existe' });
			} else {
				reject({ status: 500, message: 'Error del servidor' });
			}
		});
	}
	}
	})
	});


exports.activaUsuario = (email, token) =>

	new Promise(function(resolve, reject) {
		user.find({ email: email })

		.then(users => {

			if (users.length == 0) {

				reject({ status: 404, message: 'Usuario no encontrado' });

			} else {
				let user = users[0];
				return user;
			}
		})

		.then(user => {
			if (encriptar.encrypt(token) == user.pass_temporal) {
				user.activo = true;
				user.pass_temporal = undefined;
				user.pass_temporal_time = undefined;
				
				return user.save();
			} else {
				reject({ status: 401, message: 'Token incorrecto o caducado' });
			}
		})

		.then(user => resolve({ status: 200, message: 'Cuenta activada correctamente' }))

		.catch(err => reject({ status: 500, message: 'Error del servidor' }));

	});
	
	
		function textoAleatorio(cantidad)
		{
		    var text = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		    for( var i=0; i < cantidad; i++ )
		        text += possible.charAt(Math.floor(Math.random() * possible.length));
		    return text;
		}