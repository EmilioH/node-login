'use strict';

const user = require('../models/user');
const encriptar = require('./encriptar');

const nodemailer = require('nodemailer');
const config = require('../config/config.json');

exports.changePassword = (email, password, newPassword) => 

	new Promise(function(resolve, reject) {

		user.find({ email: email })

		.then(users => {

			let user = users[0];
			const hashed_password = user.hashed_password;
			
			 
				 
			if (encriptar.encrypt(password) == hashed_password) {
				
				if (/(.)\1{2,}/.test(newPassword)) {
           reject({ status: 409, message: 'El Password no puede tener 3 caracteres iguales seguidos' });
         } else if (newPassword.length > 10) {
         	reject({ status: 409, message: 'El nuevo Password no puede superar los 10 caracteres.' });
				 } else if (newPassword.length < 6) {
				 	reject({ status: 409, message: 'El nuevo Password debe tener al menos 6 caracteres.' });
				 } else {
				const hash = encriptar.encrypt(newPassword);
				user.hashed_password = hash;

				return user.save();
				}
			} else {

				reject({ status: 401, message: 'Password actual incorrecto' });
			}
		
		})

		.then(user => resolve({ status: 200, message: 'Password cambiado correctamente.' }))

		.catch(err => reject({ status: 500, message: 'Error del servidor' }));

	});

exports.resetPassword = email =>

	new Promise(function(resolve, reject) {

		const random = textoAleatorio(8);

		user.find({ email: email })

		.then(users => {

			if (users.length == 0) {

				reject({ status: 404, message: 'Usuario no encontrado' });

			} else {

				let user = users[0];
				const caducidad =  config.caducidad*60;
				var segundos = 0;
				
				if (user.pass_temporal_time != null) {
					const diff = new Date() - new Date(user.pass_temporal_time);
					segundos = Math.floor(diff / 1000);
				} else {
					segundos = caducidad + 1;
				}

				if (segundos > caducidad) {
					
					const hash = encriptar.encrypt(random);
					user.pass_temporal = hash;
					user.pass_temporal_time = new Date();
					return user.save();
				
				} else {
					
					reject({ status: 401, message: 'Debes esperar '+config.caducidad+' minutos para realizar otra solicitud de reseteo de Password.' });
					
				}
			}
		})

		.then(user => {
			if (user) {
			const transporter = nodemailer.createTransport(`smtps://${config.email}:${config.password}@smtp.gmail.com`);
 
			const mailOptions = {

    			from: `"${config.nombre}" <${config.email}>`,
    			to: email,  
    			subject: 'Solicitud de reseteo del Password ', 
    			html: `Hola ${user.nombre},<br><br>
    			Has solicitado resetear tu password en ${config.nombre}.<br><br>
    			Si est&aacute;s viendo este e-mail desde el dispositivo con ${config.nombre}, haz click <a href = "http://${config.url}/${random}">AQU&Iacute;</a>, de lo contrario introduce el siguiente Token en la APP: ${random}<br><br>
    			Si no has solicitado resetear tu password, puedes ignorar este e-mail.<br>
    			Este token caduca en ${config.caducidad} minutos.<br>
    			Un saludo.`
			};

			return transporter.sendMail(mailOptions);
		}
		})

		.then(info => {
			resolve({ status: 200, message: 'Revisa tu e-mail y sigue las instrucciones' })
		})

		.catch(err => {
			reject({ status: 500, message: 'Error del servidor' });

		});
	});

exports.resetPasswordFin = (email, token, password) =>

	new Promise(function(resolve, reject) {

		user.find({ email: email })

		.then(users => {

			let user = users[0];

			const diff = new Date() - new Date(user.pass_temporal_time);
			const segundos = Math.floor(diff / 1000);
			const caducidad =  config.caducidad*60;
			
			if (segundos < caducidad) {
				return user;
			} else {
				reject({ status: 401, message: 'Token caducado. Prueba de nuevo' });
			}
		})

		.then(user => {
			if (encriptar.encrypt(token) == user.pass_temporal) {
				const hash = encriptar.encrypt(password);
				user.hashed_password = hash;
				user.pass_temporal = undefined;
				user.pass_temporal_time = undefined;

				return user.save();

			} else {

				reject({ status: 401, message: 'Token incorrecto o caducado' });
			}
		})

		.then(user => resolve({ status: 200, message: 'Password cambiado correctamente' }))

		.catch(err => reject({ status: 500, message: 'Error del servidor' }));

	});
	
	
	function textoAleatorio(cantidad)
		{
		    var text = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		    for( var i=0; i < cantidad; i++ )
		        text += possible.charAt(Math.floor(Math.random() * possible.length));
		    return text;
		}