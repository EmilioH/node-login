'use strict';

const user = require('../models/user');
const encriptar = require('./encriptar');

const nodemailer = require('nodemailer');
const config = require('../config/config.json');

exports.bajaCuenta = email =>

	new Promise(function(resolve, reject) {

		const random = textoAleatorio(8);

		user.find({ email: email })

		.then(users => {

			if (users.length == 0) {

				reject({ status: 404, message: 'Usuario no encontrado' });

			} else {
					let user = users[0];
					const caducidad =  config.caducidad*60;
					var segundos = 0;
					if (user.pass_temporal_time != null) {
					//Limitamos las peticiones de baja. Si hay una pendiente, no puede solicitarla de nuevo hasta haber pasado el tiempo estimado en la configuración.
					const diff = new Date() - new Date(user.pass_temporal_time);
					segundos = Math.floor(diff / 1000);
					} else {
					segundos = caducidad+1;
					}
					if (segundos > caducidad) {
						const hash = encriptar.encrypt(random);
						user.pass_temporal = hash;
						user.pass_temporal_time = new Date();
						return user.save();
						
					} else {
						
						reject({ status: 401, message: 'Debes esperar '+config.caducidad+' minutos para realizar otra solicitud de baja.' });
					
					}
			}
		})

		.then(user => {
			if (user) {
			const transporter = nodemailer.createTransport(`smtps://${config.email}:${config.password}@smtp.gmail.com`);
 
			const mailOptions = {

    			from: `"${config.nombre}" <${config.email}>`,
    			to: email,  
    			subject: 'Solicitud de baja de tu cuenta', 
    			html: `Hola ${user.nombre},<br><br>
    			Has solicitado la baja de tu cuenta en ${config.nombre}.<br><br>
    			Si est&aacute;s viendo este e-mail desde el dispositivo con ${config.nombre}, haz click <a href = "http://${config.url}/_${random}">AQU&Iacute;</a>, de lo contrario introduce el siguiente Token en la APP: ${random}<br><br>
    			Si no has solicitado la baja de tu cuenta, puedes ignorar este e-mail.<br>
    			Este token caduca en ${config.caducidad} minutos.<br>
    			Un saludo.`
			};

			return transporter.sendMail(mailOptions);
		}
		})

		.then(info => {
			resolve({ status: 200, message: 'Revisa tu e-mail y sigue las instrucciones' })
		})

		.catch(err => {
			reject({ status: 500, message: 'Error del servidor' });

		});
	});


exports.bajaCuentaFin = (email, token) =>

	new Promise(function(resolve, reject) {

		user.find({ email: email })

		.then(users => {

			let user = users[0];

			const diff = new Date() - new Date(user.pass_temporal_time);
			const segundos = Math.floor(diff / 1000);
			const caducidad =  config.caducidad*60;
			
			if (segundos < caducidad) {
				return user;
			} else {
				reject({ status: 401, message: 'Token caducado. Prueba de nuevo' });
			}
		})

		.then(user => {
			if (encriptar.encrypt(token) == user.pass_temporal) {
				return user.remove();
			} else {
				reject({ status: 401, message: 'Token incorrecto o caducado' });
			}
		})

		.then(user => resolve({ status: 200, message: 'Cuenta borrada correctamente' }))

		.catch(err => reject({ status: 500, message: 'Error del servidor' }));

	});
	
	
	function textoAleatorio(longitud)
		{
		    var text = "";
		    var caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		    for( var i=0; i < longitud; i++ )
		        text += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
		    return text;
		}