'use strict';
const config = require('../config/config.json');
const user = require('../models/user');

exports.limpieza = function limpieza(){
    try {
			var CronJob = require('cron').CronJob;
			new CronJob(config.cronjob, function() {
				console.log("Cron de limpieza ejecutado");
			  const carpetaImagenes = config.imagenes;
					const fs = require('fs');
					fs.readdir(carpetaImagenes, (err, files) => {
					  files.forEach(file => {
					  	if (file != config.default) {
								user.find({imagen: file})
									.then(users => {
										if (users.length == 0) {
											fs.unlink(carpetaImagenes+file,function(err){
						        		if(err) return console.log('Error borrando imagen');
					   					});
										}
									})
							}
					  });
					})
			}, null, true, null);
		} catch(ex) {
		   console.log("Error ejecutando Cron de limpieza"+ex);
		}
}
 



