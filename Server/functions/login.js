'use strict';

const user = require('../models/user');
const encriptar = require('./encriptar');
const dateFormat = require('dateformat');
const nodemailer = require('nodemailer');
const config = require('../config/config.json');

exports.loginUser = (email, password) => 

	new Promise( function(resolve,reject) {
	const random = textoAleatorio(8);

		user.find({email: email})

		.then(users => {

			if (users.length == 0) {

				reject({ status: 404, message: 'Usuario o password incorrectos' });

			} else {

				return users[0];
				
			}
		})

		.then(user => {
			if (!user.activo) {
				
				const hashToken = encriptar.encrypt(random);

				const transporter = nodemailer.createTransport(`smtps://${config.email}:${config.password}@smtp.gmail.com`);

					const mailOptions = {

		    			from: `"${config.nombre}" <${config.email}>`,
		    			to: user.email,  
		    			subject: `Confirma tu cuenta en ${config.nombre}`, 
		    			html: `Hola ${user.nombre},<br><br>
		    			Gracias por registrarte en ${config.nombre}.<br><br>
		    			Para continuar con el registro, si est&aacute;s viendo este e-mail desde el dispositivo con NodeLogin, haz click <a href = "http://${config.url}/*${random}">AQU&Iacute;</a>, de lo contrario introduce el siguiente Token en la APP: ${random}<br><br>
		    			Un saludo.`
					};

				transporter.sendMail(mailOptions);
				
					user.pass_temporal = hashToken;
					user.pass_temporal_time = new Date();
					user.save();
					
				reject({ status: 401, message: 'La cuenta no ha sido activada' });
			} else {
				const hashed_password = user.hashed_password;
				if (encriptar.encrypt(password) == hashed_password) {
					var now = dateFormat(new Date(), "dd/mm/yyyy HH:MM:ss");
					user.ultimo_login = now;
					user.save();
					resolve({ status: 200, message: email });

				} else {
					reject({ status: 401, message: 'Usuario o password incorrectos' });
				}
			}
		})

		.catch(err => reject({ status: 500, message: 'Error del servidor' }));

	});
	
	
		function textoAleatorio(cantidad)
		{
		    var text = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		    for( var i=0; i < cantidad; i++ )
		        text += possible.charAt(Math.floor(Math.random() * possible.length));
		    return text;
		}