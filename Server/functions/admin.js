'use strict';

const user = require('../models/user');

exports.getListado = email =>
	
	new Promise((resolve,reject) => {
			user.find()
		
			.then(users => { return JSON.stringify(users) } )
			.then(user => { resolve({status: 200, message:user}) })

			.catch(err => reject({ status: 500, message: 'Error del servidor' }));
	});


	
	exports.actualizaUsuario = (nombre, email, nuevoEmail, administrador, activo, imagen) =>

	new Promise(function(resolve, reject) {
		user.find({ email: email })
		.then(users => {
			
			if (users.length == 0) {
				reject({ status: 404, message: 'Usuario no encontrado' });

			} else {
		
				let user = users[0];
				return user;
			}
		})

		.then(user => {
				if (nuevoEmail == "borrarCuenta") {
					return user.remove();
				} else {
				user.nombre = nombre;
				user.email = email;
				if (nuevoEmail != "" && nuevoEmail != email) {
					user.email = nuevoEmail;
				}
				user.administrador = administrador;
				user.activo = activo;
				user.imagen = imagen;
				return user.save();
				}
		})

		.then(user => resolve({ status: 200, message: 'Ficha actualizada correctamente' }))

		.catch(err => reject({ status: 500, message: 'Error del servidor' }));

	});