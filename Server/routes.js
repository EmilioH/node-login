'use strict';

const auth = require('basic-auth');
const jwt = require('jsonwebtoken');

const config = require('./config/config.json');
const login = require('./functions/login');
const registro = require('./functions/registro');
const ficha = require('./functions/ficha');
const password = require('./functions/password');
const baja = require('./functions/baja');
const admin = require('./functions/admin');
const user = require('./models/user');

var express = require('express');
var path  = require('path')
var multer  = require('multer')

var almacen = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, config.imagenes)
  }, filename: function (req, file, cb) {
  	var extensiones = [".jpg", ".jpeg", ".png"]; 
  	if (extensiones.indexOf(path.extname(file.originalname)) > -1) {
    	cb(null, (Math.random().toString(36)+'00000000000000000').slice(2, 10) + Date.now() + path.extname(file.originalname));
		}
  }
});

var upload = multer({ storage: almacen });

var router = express.Router();

	router.get('/', function(req, res) { res.end('SOLICITUD INCORRECTA') });

	router.post('/loguear', function(req, res) {
		const credentials = auth(req);
		if (!credentials) {

			res.status(400).json({ message: 'ERROR' });

		} else {

			login.loginUser(credentials.name, credentials.pass)

			.then(result => {

				const token = jwt.sign(result, config.secret, { expiresIn: 1440 });
			
				res.status(result.status).json({ message: result.message, token: token });

			})

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});

	router.post('/usuarios', function(req, res) {
		const nombre = req.body.nombre;
		const email = req.body.email;
		const password = req.body.password;

		if (!nombre || !email || !password || !nombre.trim() || !email.trim() || !password.trim()) {

			res.status(400).json({message: 'ERROR'});

		} else {
			registro.registraUsuario(nombre, email, password)

			.then(result => {
				res.setHeader('Location', '/users/'+email);
				res.status(result.status).json({ message: result.message })
			})

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});

	router.get('/usuarios/:id', function(req,res) {
		if (checkToken(req)) {

			ficha.getFicha(req.params.id)

			.then(result => res.json(result))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {

			res.status(401).json({ message: 'Token incorrecto o caducado' });
		}
	});
	
	
	router.put('/usuarios/:id', function(req,res) {

		if (checkToken(req)) {

			const oldPassword = req.body.password;
			const newPassword = req.body.newPassword;

			if (!oldPassword || !newPassword || !oldPassword.trim() || !newPassword.trim()) {

				res.status(400).json({ message: 'ERROR' });

			} else {

				password.changePassword(req.params.id, oldPassword, newPassword)

				.then(result => res.status(result.status).json({ message: result.message }))

				.catch(err => res.status(err.status).json({ message: err.message }));

			}
		} else {

			res.status(401).json({ message: 'Token incorrecto o caducado' });
		}
	});

	router.post('/usuarios/:id', function(req,res) {
		
		if (checkToken(req)) {

		const email = req.params.id;
		const token = req.body.token;

		if (!token || !token.trim()) {

			baja.bajaCuenta(email)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {

			baja.bajaCuentaFin(email, token)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));
		}

		} else {
			res.status(401).json({ message: 'Token incorrecto o caducado' });
		}
	});
	
	
	router.post('/usuarios/:id/imagen', upload.single('file'), function(req,res, next) {

		checkAdmin(req, function (authResult) {
					
			if (checkToken(req)) {
				
				const imagen = req.file.filename;
				const description = req.body.description;
				
				var usuarioUpdate = req.params.id;
				//Verificación de administración para el cambio de imágenes en otros usuarios
				if (description != "" && authResult) {
					usuarioUpdate = description;
				}
				ficha.updateImagen(usuarioUpdate,imagen)

				.then(result => res.json(result))

				.catch(err => res.status(err.status).json({ message: err.message }));

			} else {
				res.status(401).json({ message: 'Token incorrecto o caducado' });
			}
		});
	});

	
		router.post('/usuarios/:id/nombre', function(req,res) {

		if (checkToken(req)) {

			const nombre = req.body.nombre;
			
			ficha.updateNombre(req.params.id,nombre)

			.then(result => res.json(result))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {
			res.status(401).json({ message: 'Token incorrecto o caducado' });
		}
	});

	router.post('/usuarios/:id/password', function(req,res) {

		const email = req.params.id;
		const token = req.body.token;
		const newPassword = req.body.password;

		if (!token || !newPassword || !token.trim() || !newPassword.trim()) {

			password.resetPassword(email)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {

			password.resetPasswordFin(email, token, newPassword)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});

	router.post('/usuarios/:id/activacion', function(req,res) {

		const email = req.params.id;
		const token = req.body.token;

		if (!token || !token.trim()) {

			res.status(401).json({ message: 'Token incorrecto o caducado' });

		} else {

			registro.activaUsuario(email, token)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});


	function checkToken(req) {
		const token = req.headers['x-access-token'];
		if (token) {
			try {
  				var decoded = jwt.verify(token, config.secret);
  				return decoded.message === req.params.id;
			} catch(err) {
				return false;
			}
		} else {
			return false;
		}
	}



//FUNCIONES EXCLUSIVAS DE ADMINISTRACIÓN

		router.get('/lista/:id', function(req,res) {
			checkAdmin(req, function (authResult) {

		if (checkToken(req) && authResult) {
				
			admin.getListado(req.params.id)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {

			res.status(401).json({ message: 'Token incorrecto o caducado' });
		}
		});
	});
	
	
			router.post('/admin/:id/:id2', function(req,res) {
			checkAdmin(req, function (authResult) {
		if (checkToken(req) && authResult) {

			const nombre = req.body.nombre;
			const email = req.body.email;
			const nuevoEmail = req.params.id2;
			const administrador = req.body.administrador;
			const activo = req.body.activo;
			const imagen = req.body.imagen;
			
			admin.actualizaUsuario(nombre,email,nuevoEmail,administrador,activo,imagen)
			.then(result => res.json(result))
			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {
			res.status(401).json({ message: 'Token incorrecto o caducado' });
		}
				});
	});
	
	
	//Comprobación previsa de que el usuario es un administrador. Así evitamos tener que realizar comprobaciones posteriores
		function checkAdmin(req, callback)  {
	    var auth = null;

	    user.findOne({email: req.params.id},{ administrador: 1}, function(err, user) {
	        if (user === null) {
	            auth = false;
	        } else {
	            auth = user.administrador;
	        }
	        callback(auth);
	    });
	}

module.exports = router;