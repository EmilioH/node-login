'use strict';

const express    = require('express');        
const app        = express();                
const bodyParser = require('body-parser');
const port 	   = process.env.PORT || 5000;

app.use(bodyParser.json());

var router = require('./routes');
var cron = require('./functions/cron');

app.use('/', router);
app.use("/images", express.static(__dirname + '/images'));

app.listen(port);

//Borrado regular de imagenes. Lee las notas en ficha.js
cron.limpieza();

console.log(`NodeLogin funcionando en el puerto ${port}`);